const os = require('os')
const cli = require('./lib/cli')
const cluster = require('cluster')
const servers = require('./lib/servers')
const workers = require('./lib/workers')

let app = {}

app.init = (callback) => {
    if (cluster.isMaster) {
        workers.init()
    
        setTimeout(() => {
            cli.init()
    
            callback()
        }, 50)

        for (let index = 0; index < os.cpus().length; index++) {
            cluster.fork()
        }
    } else {
        servers.init()
    }
}

if (require.main === module) {
    app.init(() => {})
}

module.exports = app