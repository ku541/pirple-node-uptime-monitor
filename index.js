const servers = require('./lib/servers')
const workers = require('./lib/workers')
const cli = require('./lib/cli')

let app = {}

app.init = (callback) => {
    servers.init()

    workers.init()

    setTimeout(() => {
        cli.init()

        callback()
    }, 50)
}

if (require.main === module) {
    app.init(() => {})
}

module.exports = app