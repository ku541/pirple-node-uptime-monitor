const url = require('url')
const http = require('http')
const https = require('https')
const _data = require('./data')
const _logs = require('./logs')
const helpers = require('./helpers')

let workers = {}

workers.gatherAllChecks = () => {
    _data.list('checks', (error, checks) => {
        if (!error && checks && checks.length > 0) {
            checks.forEach((check) => {
                _data.read('checks', check, (error, checkData) => {
                    if (!error && checkData) {
                        workers.validateCheck(checkData)
                    } else {
                        console.log(
                            '\x1b[31m%s\x1b[0m',
                            'Error: Could not read check data'
                        )
                    }
                })
            })
        } else {
            console.log(
                '\x1b[31m%s\x1b[0m',
                'Error: Could not find any checks to process'
            )
        }
    })
}

workers.validateCheck = (check) => {
    check = typeof check == 'object' && check !== null ? check : {}

    check.id = typeof check.id == 'string' && check.id.trim().length == 20 ? check.id.trim() : false
    check.userPhone = typeof check.userPhone == 'string' && check.userPhone.trim().length == 10 ? check.userPhone.trim() : false
    check.protocol = typeof check.protocol == 'string' && ['http', 'https'].includes(check.protocol) ? check.protocol : false
    check.url = typeof check.url == 'string' && check.url.trim().length > 0 ? check.url.trim() : false
    check.method = typeof check.method == 'string' && ['post', 'get', 'put', 'delete'].includes(check.method) ? check.method : false
    check.successCodes = typeof check.successCodes == 'object' && check.successCodes instanceof Array && check.successCodes.length > 0 ? check.successCodes : false
    check.timeoutSeconds = typeof check.timeoutSeconds == 'number' && check.timeoutSeconds % 1 == 0 && check.timeoutSeconds >= 1 && check.timeoutSeconds <= 5 ? check.timeoutSeconds : false

    check.state = typeof check.state == 'string' && ['up', 'down'].includes(check.state) ? check.state : 'down'
    check.lastChecked = typeof check.lastChecked == 'number' && check.lastChecked > 0 ? check.lastChecked : Date.now()

    if (
        check.id &&
        check.userPhone &&
        check.protocol &&
        check.url &&
        check.method &&
        check.successCodes &&
        check.timeoutSeconds &&
        check.state &&
        check.lastChecked
    ) {
        workers.performCheck(check)
    } else {
        console.log(
            '\x1b[31m%s\x1b[0m',
            'Error: Could not perform check due to insufficient data'
        )
    }
}

workers.performCheck = (check) => {
    let outcome = {
        'error': false,
        'responseCode': false
    }

    let outcomeSent = false

    let parsedUrl = url.parse(check.protocol + '://' + check.url, true)

    let hostname = parsedUrl.hostname
    let path = parsedUrl.path

    let requestDetails = {
        'protocol': check.protocol + ':',
        'hostname': hostname,
        'method': check.method.toUpperCase(),
        'path': path,
        'timeout': check.timeoutSeconds * 1000
    }

    let protocol = check.protocol == 'http' ? http : https

    let request = protocol.request(requestDetails, (response) => {
        let status = response.statusCode

        outcome.responseCode = status

        if (!outcomeSent) {
            workers.processOutcome(check, outcome)

            outcomeSent = true
        }
    })

    request.on('error', (error) => {
        outcome.error = {
            'error': true,
            'value': error
        }

        if (!outcomeSent) {
            workers.processOutcome(check, outcome)

            outcomeSent = true
        }
    })

    request.on('timeout', () => {
        outcome.error = {
            'error': true,
            'value': 'timeout'
        }

        if (!outcomeSent) {
            workers.processOutcome(check, outcome)

            outcomeSent = true
        }
    })

    request.end()
}

workers.processOutcome = (check, outcome) => {
    let state = !outcome.error && outcome.responseCode &&
        check.successCodes.includes(outcome.responseCode) ? 'up' : 'down'

    let alertWarranted = check.lastChecked && check.state !== state ? true : false

    let timeOfCheck = Date.now()

    workers.log(check, outcome, state, alertWarranted, timeOfCheck)

    check.state = state
    check.lastChecked = timeOfCheck

    _data.update('checks', check.id, check, (error) => {
        if (!error) {
            if (alertWarranted) {
                workers.alertUser(check)
            }
        } else {
            console.log(
                '\x1b[31m%s\x1b[0m',
                'Error: Could not save updates to one of the checks'
            )
        }
    })
}

workers.alertUser = (check) => {
    let message = `Alert: Your check for ${check.method.toUpperCase()} ${check.protocol.toUpperCase()}://${check.url} is currently ${check.state}`

    helpers.sendTwilioSms(check.userPhone, message, (error) => {
        if (error) {
            console.log(
                '\x1b[31m%s\x1b[0m',
                'Error: Could not send text message to user who had a state change in their check'
            )
        }
    })
}

workers.log = (check, outcome, state, alert, time) => {
    let logData = {
        check,
        outcome,
        state,
        alert,
        time
    }

    let logString = JSON.stringify(logData)

    let logFileName = check.id

    _logs.append(logFileName, logString, (error) => {
        if (error) {
            console.log('\x1b[31m%s\x1b[0m', 'Error: Could not log to file')
        }
    })
}

workers.loop = () => {
    setInterval(() => workers.gatherAllChecks(), 1000 * 60)
}

workers.rotateLogs = () => {
    _logs.list(false, (error, logs) => {
        if (!error && logs && logs.length > 0) {
            logs.forEach((logName) => {
                let logId = logName.replace('.log', '')

                let newFileId = logId + '-' + Date.now()

                _logs.compress(logId, newFileId, (error) => {
                    if (!error) {
                        _logs.truncate(logId, (error) => {
                            if (error) {
                                console.log(
                                    '\x1b[31m%s\x1b[0m',
                                    'Error: Could not truncate log file'
                                )
                            }
                        })
                    } else {
                        console.log(
                            '\x1b[31m%s\x1b[0m',
                            `Error: Could not compress log file - ${error}`
                        )
                    }
                })
            })
        } else {
            console.log(
                '\x1b[31m%s\x1b[0m',
                'Error: Could not find any logs to rotate'
            )
        }
    })
}

workers.logRotationLoop = () => {
    setInterval(() => workers.gatherAllChecks(), 1000 * 60 * 60 * 24)
}

workers.init = () => {
    workers.gatherAllChecks()

    workers.loop()

    workers.rotateLogs()

    workers.logRotationLoop()
}

module.exports = workers