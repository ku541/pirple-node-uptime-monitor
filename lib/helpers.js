const fs = require('fs')
const path = require('path')
const https = require('https')
const crypto = require('crypto')
const querystring = require('querystring')
const config = require('./config')

let helpers = {}

helpers.hash = (str) => {
    if (typeof str == 'string' && str.length > 0) {
        let hash = crypto.createHmac('sha256', config.hashingSecret)
            .update(str)
            .digest('hex')

        return hash
    } else {
        return false
    }
}

helpers.parseJsonToObject = (str) => {
    try {
        return JSON.parse(str)
    } catch (e) {
        return {}
    }
}

helpers.createRandomString = (strLength) => {
    strLength = typeof strLength == 'number' && strLength > 0 ? strLength : false

    if (strLength) {
        let possibleCharacters = 'abcdefghijklmnopqrstuvwxyz0123456789'

        let str = ''

        for (let i = 1; i <= strLength; i++) {
            let randomCharacter = possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length))

            str += randomCharacter
        }

        return str

    } else {
        return false
    }
}

helpers.sendTwilioSms = (phone, message, callback) => {
    phone = typeof phone == 'string' && phone.trim().length == 10 ? phone.trim() : false
    message = typeof message == 'string' && message.trim().length > 0 && message.trim().length <= 1600 ? message.trim() : false

    if (phone && message) {
        let payload = {
            'From': config.twilio.fromPhone,
            'To': '+94' + phone,
            'Body': message
        }

        let stringPayload = querystring.stringify(payload)

        let requestDetails = {
            'protocol': 'https:',
            'hostname': 'api.twilio.com',
            'method': 'POST',
            'path': '/2010-04-01/Accounts/' + config.twilio.accountSid + '/Messages.json',
            'auth': config.twilio.accountSid + ':' + config.twilio.authToken,
            'header': {
                'Content-Tyoe': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(stringPayload)
            }
        }

        let request = https.request(requestDetails, (response) => {
            let status = response.statusCode

            if (status == 200 || status == 201) {
                callback(false)
            } else {
                // callback('Status code returned was ' + status)
                callback(false)
            }
        })

        request.on('error', (error) => {
            callback(error)
        })

        request.write(stringPayload)

        request.end()
    } else {
        callback('Given parameters were missing or invalid')
    }
}

helpers.getTemplate = (name, data, callback) => {
    name = typeof name == 'string' && name.length > 0 ? name : false
    data = typeof data == 'object' && data !== null ? data : {}

    if (name) {
        let templatesDir = path.join(__dirname, '/../templates/')

        fs.readFile(templatesDir + name + '.html', 'utf8', (error, template) => {
            if (! error && template && template.length > 0) {
                template = helpers.interpolate(template, data)

                callback(false, template)
            } else {
                callback('No template could be found')
            }
        })
    } else {
        callback('A valid template name was not specified')
    }
}

helpers.addUniversalTemplates = (template, data, callback) => {
    template = typeof template == 'string' && template.length > 0 ? template : ''
    data = typeof data == 'object' && data !== null ? data : {}

    helpers.getTemplate('_header', data, (error, header) => {
        if (! error && header) {
            helpers.getTemplate('_footer', data, (error, footer) => {
                if (! error && footer) {
                    template = header + template + footer

                    callback(false, template)
                } else {
                    callback('Could not find the footer template')
                }
            })
        } else {
            callback('Could not find the header template')
        }
    })
}

helpers.interpolate = (template, data) => {
    template = typeof template == 'string' && template.length > 0 ? template : ''
    data = typeof data == 'object' && data !== null ? data : {}

    for (const key in config.templateGlobals) {
        if (Object.hasOwnProperty.call(config.templateGlobals, key)) {
            data[`global.${key}`] = config.templateGlobals[key]
        }
    }

    for (const key in data) {
        if (Object.hasOwnProperty.call(data, key) && typeof data[key] == 'string') {
            const replace = data[key]
            const find = `{${key}}`

            template = template.replace(find, replace)
        }
    }

    return template
}

helpers.getStaticAsset = (fileName, callback) => {
    fileName = typeof fileName == 'string' && fileName.length > 0 ? fileName : false

    if (fileName) {
        let publicDir = path.join(__dirname, '/../public/')

        fs.readFile(publicDir + fileName, (error, asset) => {
            if (! error && asset) {
                callback(false, asset)
            } else {
                callback('No file could be found')
            }
        })
    } else {
        callback('A valid filename was not specified')
    }
}

module.exports = helpers