const fs = require('fs')
const path = require('path')
const zlib = require('zlib')

let lib = {}

lib.baseDir = path.join(__dirname, '/../.logs/')

lib.append = (file, string, callback) => {
    fs.open(lib.baseDir + file + '.log', 'a', (error, fileDescriptor) => {
        if (!error && fileDescriptor) {
            fs.appendFile(fileDescriptor, string + '\n', (error) => {
                if (!error) {
                    fs.close(fileDescriptor, (error) => {
                        if (!error) {
                            callback(false)
                        } else {
                            callback('Error: Could not close file that was being appended')
                        }
                    })
                } else {
                    callback('Error: Could not append to file')
                }
            })
        } else {
            callback('Error: Could not open file for appending')
        }
    })
}

lib.list = (includeCompressedLogs, callback) => {
    fs.readdir(lib.baseDir, (error, data) => {
        if (!error && data && data.length > 0) {
            let trimmedFileNames = []

            data.forEach((fileName) => {
                if (fileName.includes('.log')) {
                    trimmedFileNames.push(fileName.replace('.log', ''))
                }

                if (fileName.includes('.gz.b64') && includeCompressedLogs) {
                    trimmedFileNames.push(fileName.replace('.gz.b64', ''))
                }
            })

            callback(false, trimmedFileNames)
        } else {
            callback(error, data)
        }
    })
}

lib.compress = (logId, compressedLogId, callback) => {
    let logFileName = logId + '.log'
    let destinationFileName = compressedLogId + '.gz.b64'

    fs.readFile(lib.baseDir + logFileName, 'utf8', (error, logContents) => {
        if (!error && logContents) {
            zlib.gzip(logContents, (error, buffer) => {
                if (!error && buffer) {
                    fs.open(lib.baseDir + destinationFileName, 'wx', (error, fileDescriptor) => {
                        if (!error && fileDescriptor) {
                            fs.writeFile(fileDescriptor, buffer.toString('base64'), (error) => {
                                if (!error) {
                                    fs.close(fileDescriptor, (error) => {
                                        if (!error) {
                                            callback(false)
                                        } else {
                                            callback(error)
                                        }
                                    })
                                } else {
                                    callback(error)
                                }
                            })
                        } else {
                            callback(error)
                        }
                    })
                } else {
                    callback(error)
                }
            })
        } else {
            callback(error)
        }
    })
}

lib.decompress = (compressedLogId, callback) => {
    let compressedFileName = compressedLogId + '.gz.b64'

    fs.readFile(lib.baseDir + compressedFileName, 'utf8', (error, compressedContents) => {
        if (!error && compressedContents) {
            let compressedContentsBuffer = Buffer.from(compressedContents, 'base64')

            zlib.unzip(compressedContentsBuffer, (error, contentsBuffer) => {
                if (!error && contentsBuffer) {
                    let contents = contentsBuffer.toString()

                    callback(false, contents)
                } else {
                    callback(error)
                }
            })
        } else {
            callback(error)
        }
    })
}

lib.truncate = (logId, callback) => {
    fs.truncate(lib.baseDir + logId + '.log', 0, (error) => {
        if (!error) {
            callback(false)
        } else {
            callback(error)
        }
    })
}

module.exports = lib