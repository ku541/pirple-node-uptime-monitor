const readline = require('readline')
const util = require('util')
const debug = util.debuglog('cli')
const events = require('events')
const os = require('os')
const v8 = require('v8')
const _data = require('./data')
const _logs = require('./logs')
const _helpers = require('./helpers')
const child_process = require('child_process')

class _events extends events { }

const e = new _events

let cli = {}

e.on('man', (input) => {
    cli.responders.help()
})

e.on('help', (input) => {
    cli.responders.help()
})

e.on('exit', (input) => {
    cli.responders.exit()
})

e.on('stats', (input) => {
    cli.responders.stats()
})

e.on('list users', (input) => {
    cli.responders.listUsers()
})

e.on('show user', (input) => {
    cli.responders.showUser(input)
})

e.on('list checks', (input) => {
    cli.responders.listChecks(input)
})

e.on('show check', (input) => {
    cli.responders.showCheck(input)
})

e.on('list logs', (input) => {
    cli.responders.listLogs()
})

e.on('show log', (input) => {
    cli.responders.showLog(input)
})

cli.responders = {}

cli.responders.help = () => {
    const commands = {
        'exit': 'Kill the CLI (& the rest of the application)',
        'man': 'Show this help page',
        'help': 'Alias of the "man" command',
        'stats': 'List statistics of the underlying operating system & resource utilization',
        'list users': 'List all the registered (undeleted) users in the system',
        'show user --{userId}': 'Show details of the specified user',
        'list checks --up --down': 'List all the active checks in the system including their state. The "--up" & the "--down" flags are both optional',
        'show check --{checkId}': 'Show details of the specified check',
        'list logs': 'List all the log files available to be read (compressed only)',
        'show log --{fileName}': 'Show details of the specified log file'
    }

    cli.horizontalLine()

    cli.centered('CLI MANUAL')

    cli.horizontalLine()

    cli.verticalSpace(2)

    for (const command in commands) {
        if (Object.hasOwnProperty.call(commands, command)) {
            const description = commands[command]
            let line = '\x1b[32m' + command + '\x1b[0m'
            const padding = 60 - line.length

            for (let i = 0; i < padding; i++) {
                line += ' '
            }

            line += description

            console.log(line)

            cli.verticalSpace()
        }
    }

    cli.verticalSpace(1)

    cli.horizontalLine()
}

cli.verticalSpace = (lines) => {
    lines = typeof lines === 'number' && lines > 0 ? lines : 1

    for (let i = 0; i < lines; i++) {
        console.log('')
    }
}

cli.horizontalLine = () => {
    const width = process.stdout.columns
    let line = ''

    for (let i = 0; i < width; i++) {
        line += '-'
    }

    console.log(line)
}

cli.centered = (str) => {
    str = typeof str === 'string' && str.trim().length > 0 ? str.trim() : ''

    const width = process.stdout.columns
    const leftPadding = Math.floor((width - str.length) / 2)
    let line = ''

    for (let i = 0; i < leftPadding; i++) {
        line += ' '
    }

    line += str

    console.log(line)
}

cli.responders.exit = () => {
    process.exit(0)
}

cli.responders.stats = () => {
    const stats = {
        'Load Average': os.loadavg().join(' '),
        'CPU Count': os.cpus().length,
        'Free Memory': os.freemem(),
        'Current Malloced Memory': v8.getHeapStatistics().malloced_memory,
        'Peak Malloced Memory': v8.getHeapStatistics().peak_malloced_memory,
        'Allocated Heap Used (%)': Math.round((v8.getHeapStatistics().used_heap_size / v8.getHeapStatistics().total_heap_size) * 100),
        'Available Heap Allocated (%)': Math.round((v8.getHeapStatistics().total_heap_size / v8.getHeapStatistics().heap_size_limit) * 100),
        'Uptime': os.uptime() + ' Seconds'
    }

    cli.horizontalLine()

    cli.centered('SYSTEM STATISTICS')

    cli.horizontalLine()

    cli.verticalSpace(2)

    for (const stat in stats) {
        if (Object.hasOwnProperty.call(stats, stat)) {
            const description = stats[stat]
            let line = '\x1b[32m' + stat + '\x1b[0m'
            const padding = 60 - line.length

            for (let i = 0; i < padding; i++) {
                line += ' '
            }

            line += description

            console.log(line)

            cli.verticalSpace()
        }
    }

    cli.verticalSpace(1)
}

cli.responders.listUsers = () => {
    _data.list('users', (error, userIds) => {
        if (! error && userIds && userIds.length > 0) {
            cli.verticalSpace()

            userIds.forEach((userId) => {
                _data.read('users', userId, (error, user) => {
                    if (! error && user) {
                        let line = `Name: ${user.firstName} ${user.lastName} Phone: ${user.phone} Checks: `
                        
                        let checksCount = typeof user.checks === 'object' && user.checks instanceof Array && user.checks.length > 0 ? user.checks.length : 0

                        line += checksCount

                        console.log(line)

                        cli.verticalSpace()
                    }
                })
            })
        }
    })
}

cli.responders.showUser = (input) => {
    const split = input.split('--')
    const userId = typeof split[1] === 'string' && split[1].length > 0 ? split[1].trim() : false

    if (userId) {
        _data.read('users', userId, (error, user) => {
            if (! error && user) {
                delete user.hashedPassword

                cli.verticalSpace()

                console.dir(user, { 'colors': true })

                cli.verticalSpace()
            }
        })
    }
}

cli.responders.listChecks = (input) => {
    _data.list('checks', (error, checkIds) => {
        if (! error && checkIds && checkIds.length > 0) {
            cli.verticalSpace()

            checkIds.forEach((checkId) => {
                _data.read('checks', checkId, (error, check) => {
                    if (! error && typeof check === 'object') {
                        let includeCheck = false
                        input = input.toLowerCase()

                        let state = typeof check.state === 'string' ? check.state : 'down'
                        let stateOrUnknown = typeof check.state === 'string' ? check.state : 'unknown'

                        if (input.includes(`--${state}`) || ! input.includes('--down') && ! input.includes('--up')) {
                            let line = `ID: ${check.id} ${check.method.toUpperCase()} ${check.protocol}://${check.url} State: ${stateOrUnknown}`

                            console.log(line)

                            cli.verticalSpace()
                        }
                    }
                })
            })
        }
    })
}

cli.responders.showCheck = (input) => {
    const split = input.split('--')
    const checkId = typeof split[1] === 'string' && split[1].length > 0 ? split[1].trim() : false

    if (checkId) {
        _data.read('checks', checkId, (error, check) => {
            if (! error && check) {
                cli.verticalSpace()

                console.dir(check, { 'colors': true })

                cli.verticalSpace()
            }
        })
    }
}

cli.responders.listLogs = () => {
    // _logs.list(true, (error, logFilenames) => {
    //     if (! error && logFilenames && logFilenames.length > 0) {
    //         cli.verticalSpace()

    //         logFilenames.forEach((logFilename) => {
    //             if (logFilename.includes('-')) {
    //                 console.log(logFilename);
    //             }
    //         })

    //         cli.verticalSpace()
    //     }
    // })

    const ls = child_process.spawn('ls', ['./.logs/']);

    ls.stdout.on('data', (data) => {
       const logFilenames = data.toString().split('\n')

        cli.verticalSpace()

        logFilenames.forEach((logFilename) => {
            if (typeof logFilename === 'string' && logFilename.length > 0 && logFilename.includes('-')) {
                console.log(logFilename.trim().split('.')[0]);
            }
        })

        cli.verticalSpace()
    });
}

cli.responders.showLog = (input) => {
    const split = input.split('--')
    const logFilename = typeof split[1] === 'string' && split[1].length > 0 ? split[1].trim() : false

    if (logFilename) {
        cli.verticalSpace()

        _logs.decompress(logFilename, (error, log) => {
            if (! error && log) {
                let logLines = log.split('\n')

                logLines.forEach((logLine) => {
                    let logObject = _helpers.parseJsonToObject(logLine)

                    if (logObject && JSON.stringify(logObject) !== '{}') {
                        console.dir(logObject, { 'colors': true })

                        cli.verticalSpace()
                    }
                })
            }
        })
    }
}

cli.processInput = (input) => {
    input = typeof input == 'string' && input.trim().length > 0 ? input.trim() : false

    if (input) {
        const uniqueInputs = [
            'man',
            'help',
            'exit',
            'stats',
            'list users',
            'show user',
            'list checks',
            'show check',
            'list logs',
            'show log'
        ]

        let matchFound = false
        let counter = 0

        uniqueInputs.some((uniqueInput) => {
            if (input.includes(uniqueInput)) {
                matchFound = true

                e.emit(uniqueInput, input)

                return true
            }
        })

        if (! matchFound) {
            console.log('Sorry, try again')
        }
    }
}

cli.init = () => {
    console.log('\x1b[36m%s\x1b[0m', 'The CLI is running')

    let _interface = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        prompt: '> '
    })

    _interface.prompt()

    _interface.on('line', (input) => {
        cli.processInput(input)

        _interface.prompt()
    })

    _interface.on('close', () => process.exit(0))
}

module.exports = cli