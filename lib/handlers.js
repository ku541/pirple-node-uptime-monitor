const dns = require('dns')
const _url = require('url')
const util = require('util')
const _data = require('./data')
const config = require('./config')
const helpers = require('./helpers')
const debug = util.debuglog('performance')
const { performance, PerformanceObserver } = require('perf_hooks')

const performanceObserver = new PerformanceObserver((list, observer) => {
    const measurements = list.getEntriesByType('measure')

    measurements.forEach(measurement => console.log('\x1b[37m%s\x1b[0m', `${measurement.name} ${measurement.duration}`))

    observer.disconnect()
})

performanceObserver.observe({ entryTypes: ['measure'], buffered: true })

let handlers = {}

handlers.index = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Uptime Monitoring - Made Simple',
            'head.description': 'We offer free, simple uptime monitoring for HTTP/HTTPS sites of all kinds. When your site goes down we will send you a text message to let you know.',
            'body.class': 'index'
        }

        helpers.getTemplate('index', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {
                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.accountCreate = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Create Your Account',
            'head.description': 'Sign up is easy and only takes a few seconds.',
            'body.class': 'accountCreate'
        }

        helpers.getTemplate('accountCreate', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {
                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.sessionCreate = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Log in to Your Account',
            'head.description': 'Please enter your phone number and password to access your account.',
            'body.class': 'sessionCreate'
        }

        helpers.getTemplate('sessionCreate', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {
                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.sessionDeleted = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Logged Out',
            'head.description': 'You have been logged out of your account.',
            'body.class': 'sessionDeleted'
        }

        helpers.getTemplate('sessionDeleted', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {
                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.accountEdit = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Account Settings',
            'body.class': 'accountEdit'
        }

        helpers.getTemplate('accountEdit', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {
                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.accountDeleted = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Account Deleted',
            'head.description': 'Your account has been deleted',
            'body.class': 'accountDeleted'
        }

        helpers.getTemplate('accountDeleted', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {

                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.checksCreate = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Create a New Check',
            'body.class': 'checksCreate'
        }

        helpers.getTemplate('checksCreate', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {

                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.checksList = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Dashboard',
            'body.class': 'checksList'
        }

        helpers.getTemplate('checksList', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {

                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.checksEdit = (data, callback) => {
    if (data.method == 'get') {
        let data = {
            'head.title': 'Check Details',
            'body.class': 'checksEdit'
        }

        helpers.getTemplate('checksEdit', data, (error, template) => {
            if (! error && template) {
                helpers.addUniversalTemplates(template, data, (error, html) => {

                    if (! error && html) {
                        callback(200, html, 'html')
                    } else {
                        callback(500, undefined, 'html')
                    }
                })
            } else {
                callback(500, undefined, 'html')
            }
        })
    } else {
        callback(405, undefined, 'html')
    }
}

handlers.favicon = (data, callback) => {
    if (data.method == 'get') {
        helpers.getStaticAsset('favicon.ico', (error, favicon) => {
            if (! error && favicon) {
                callback(200, favicon, 'favicon')
            } else {
                callback(500)
            }
        })
    } else {
        callback(405)
    }
}

handlers.public = (data, callback) => {
    if (data.method == 'get') {
        let trimmedAssetName = data.trimmedPath.replace('public/', '').trim()

        if (trimmedAssetName.length > 0) {
            helpers.getStaticAsset(trimmedAssetName, (error, asset) => {
                if (! error && asset) {
                    let contentType = 'plain'

                    if (trimmedAssetName.includes('.css')) {
                        contentType = 'css'
                    }

                    if (trimmedAssetName.includes('.png')) {
                        contentType = 'png'
                    }

                    if (trimmedAssetName.includes('.jpg')) {
                        contentType = 'jpg'
                    }

                    if (trimmedAssetName.includes('.ico')) {
                        contentType = 'favicon'
                    }

                    callback(200, asset, contentType)
                } else {
                    callback(404)
                }
            })
        } else {
            callback(404)
        }
    } else {
        callback(405)
    }
}

handlers.exampleError = () => {
    const error = new Error('This is an example error')

    throw error
}

handlers.users = (data, callback) => {
    let acceptableMethods = ['post', 'get', 'put', 'delete']

    if (acceptableMethods.includes(data.method)) {
        handlers._users[data.method](data, callback)
    } else {
        callback(405)
    }
}

handlers._users = {}

handlers._users.post = (data, callback) => {
    let firstName = typeof data.payload.firstName == 'string' && data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false
    let lastName = typeof data.payload.lastName == 'string' && data.payload.lastName.trim().length > 0 ? data.payload.lastName.trim() : false
    let phone = typeof data.payload.phone == 'string' && data.payload.phone.trim().length == 10 ? data.payload.phone.trim() : false
    let password = typeof data.payload.password == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false
    let tosAgreement = typeof data.payload.tosAgreement == 'boolean' && data.payload.tosAgreement == true

    if (firstName && lastName && phone && password && tosAgreement) {
        _data.read('users', phone, (err) => {
            if (err) {
                let hashedPassword = helpers.hash(password)

                if (hashedPassword) {
                    let userObject = {
                        'firstName': firstName,
                        'lastName': lastName,
                        'phone': phone,
                        'hashedPassword': hashedPassword,
                        'tosAgreement': tosAgreement
                    }

                    _data.create('users', phone, userObject, (err) => {
                        if (!err) {
                            callback(200)
                        } else {
                            console.log('\x1b[31m%s\x1b[0m', err)

                            callback(500, { 'Error': 'Could not create the new user' })
                        }
                    })
                } else {
                    callback(500, { 'Error': 'Could not hash the user\'s password' })
                }
            } else {
                callback(400, {
                    'Error': 'A user with that phone number already exists'
                })
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required fields' })
    }
}

handlers._users.get = (data, callback) => {
    let phone = typeof data.queryStringObject.phone == 'string' && data.queryStringObject.phone.trim().length == 10 ? data.queryStringObject.phone.trim() : false

    if (phone) {
        let token = typeof data.headers.token == 'string' ? data.headers.token : false

        handlers._tokens.verifyToken(token, phone, (tokenIsValid) => {
            if (tokenIsValid) {
                _data.read('users', phone, (err, data) => {
                    if (!err && data) {
                        delete data.hashedPassword

                        callback(200, data)
                    } else {
                        callback(404)
                    }
                })
            } else {
                callback(403, { 'Error': 'Missing required token in header, or token is invalid' })
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required field' })
    }
}

handlers._users.put = (data, callback) => {
    let phone = typeof data.payload.phone == 'string' && data.payload.phone.trim().length == 10 ? data.payload.phone.trim() : false
    let firstName = typeof data.payload.firstName == 'string' && data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false
    let lastName = typeof data.payload.lastName == 'string' && data.payload.lastName.trim().length > 0 ? data.payload.lastName.trim() : false
    let password = typeof data.payload.password == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false

    if (phone) {
        if (firstName || lastName || password) {
            let token = typeof data.headers.token == 'string' ? data.headers.token : false

            handlers._tokens.verifyToken(token, phone, (tokenIsValid) => {
                if (tokenIsValid) {
                    _data.read('users', phone, (err, data) => {
                        if (!err && data) {
                            if (firstName) {
                                data.firstName = firstName
                            }

                            if (lastName) {
                                data.lastName = lastName
                            }

                            if (password) {
                                data.hashedPassword = helpers.hash(password)
                            }

                            _data.update('users', phone, data, (err,) => {
                                if (!err) {
                                    callback(200)
                                } else {
                                    console.log('\x1b[31m%s\x1b[0m', err)

                                    callback(500, { 'Error': 'Could not update the user' })
                                }
                            })
                        } else {
                            callback(400, { 'Error': 'The specified user does not exist' })
                        }
                    })
                } else {
                    callback(403, { 'Error': 'Missing required token in header, or token is invalid' })
                }
            })
        } else {
            callback(400, { 'Error': 'Missing fields to update' })
        }
    } else {
        callback(400, { 'Error': 'Missing required field' })
    }
}

handlers._users.delete = (data, callback) => {
    let phone = typeof data.queryStringObject.phone == 'string' && data.queryStringObject.phone.trim().length == 10 ? data.queryStringObject.phone.trim() : false

    if (phone) {
        let token = typeof data.headers.token == 'string' ? data.headers.token : false

        handlers._tokens.verifyToken(token, phone, (tokenIsValid) => {
            if (tokenIsValid) {
                _data.read('users', phone, (err, user) => {
                    if (!err && user) {
                        _data.delete('users', phone, (err) => {
                            if (!err) {
                                let checks = typeof user.checks == 'object' && user.checks instanceof Array ? user.checks : []

                                if (checks.length > 0) {
                                    let checksDeleted = 0
                                    let deletionErrors = false

                                    checks.forEach((checkId) => {
                                        _data.delete('checks', checkId, (error) => {
                                            if (error) {
                                                deletionErrors = true
                                            }

                                            ++checksDeleted

                                            if (checksDeleted == checks.length) {
                                                if (!deletionErrors) {
                                                    callback(200)
                                                } else {
                                                    callback(500, { 'Error': 'Errors encountered while attempting to delete all of the user\'s checks. All checks may not have been deleted from the system successfully' })
                                                }
                                            }
                                        })
                                    })
                                } else {
                                    callback(200)
                                }
                            } else {
                                callback(500, { 'Error': 'Could not delete the specified user' })
                            }
                        })
                    } else {
                        callback(400, { 'Error': 'Could not find the specified user' })
                    }
                })
            } else {
                callback(403, { 'Error': 'Missing required token in header, or token is invalid' })
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required field' })
    }
}

handlers.tokens = (data, callback) => {
    let acceptableMethods = ['post', 'get', 'put', 'delete']

    if (acceptableMethods.includes(data.method)) {
        handlers._tokens[data.method](data, callback)
    } else {
        callback(405)
    }
}

handlers._tokens = {}

handlers._tokens.post = (data, callback) => {
    performance.mark('entered function')

    let phone = typeof data.payload.phone == 'string' && data.payload.phone.trim().length == 10 ? data.payload.phone.trim() : false
    let password = typeof data.payload.password == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false

    performance.mark('inputs validated')

    if (phone && password) {
        performance.mark('beginning user lookup')

        _data.read('users', phone, (err, data) => {
            performance.mark('user lookup complete')

            if (!err && data) {
                performance.mark('beginning password hashing')

                let hashedPassword = helpers.hash(password)

                performance.mark('password hashing complete')

                if (hashedPassword == data.hashedPassword) {
                    performance.mark('creating data for token')

                    let tokenId = helpers.createRandomString(20)
                    let expires = Date.now() + 1000 * 60 * 60

                    let tokenObject = {
                        'phone': phone,
                        'id': tokenId,
                        'expires': expires
                    }

                    performance.mark('beginning storing token')

                    _data.create('tokens', tokenId, tokenObject, (err) => {
                        performance.mark('storing token complete')

                        performance.measure('Beginning to end', 'entered function', 'storing token complete')
                        performance.measure('Validating user input', 'entered function', 'inputs validated')
                        performance.measure('User lookup', 'beginning user lookup', 'user lookup complete')
                        performance.measure('Password hashing', 'beginning password hashing', 'password hashing complete')
                        performance.measure('Token data creation', 'creating data for token', 'beginning storing token')
                        performance.measure('Token storing', 'beginning storing token', 'storing token complete')

                        if (!err) {
                            callback(200, tokenObject)
                        } else {
                            callback(500, { 'Error': 'Could not create the new token' })
                        }
                    })
                } else {
                    callback(400, { 'Error': 'Password did not match the specified user\'s stored password' })
                }
            } else {
                callback(400, { 'Error': 'Could not find the specified user' })
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required field(s)' })
    }
}

handlers._tokens.get = (data, callback) => {
    let id = typeof data.queryStringObject.id == 'string' && data.queryStringObject.id.trim().length == 20 ? data.queryStringObject.id.trim() : false

    if (id) {
        _data.read('tokens', id, (err, data) => {
            if (!err && data) {
                callback(200, data)
            } else {
                callback(404)
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required field' })
    }
}

handlers._tokens.put = (data, callback) => {
    let id = typeof data.payload.id == 'string' && data.payload.id.trim().length == 20 ? data.payload.id.trim() : false
    let extend = typeof data.payload.extend == 'boolean' && data.payload.extend == true ? data.payload.extend : false

    if (id && extend == true) {
        _data.read('tokens', id, (err, data) => {
            if (!err && data) {
                if (data.expires > Date.now()) {
                    data.expires = Date.now() + 1000 * 60 * 60

                    _data.update('tokens', id, data, (err) => {
                        if (!err) {
                            callback(200)
                        } else {
                            callback(500, { 'Error': 'Could not update the token\'s expiration' })
                        }
                    })
                } else {
                    callback(400, { 'Error': 'The token has already expired, and cannot be extended' })
                }
            } else {
                callback(400, { 'Error': 'Specified token does not exist' })
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required field(s) or field(s) are invalid.' })
    }
}

handlers._tokens.delete = (data, callback) => {
    let id = typeof data.queryStringObject.id == 'string' && data.queryStringObject.id.trim().length == 20 ? data.queryStringObject.id.trim() : false

    if (id) {
        _data.read('tokens', id, (err, data) => {
            if (!err && data) {
                _data.delete('tokens', id, (err) => {
                    if (!err) {
                        callback(200)
                    } else {
                        callback(500, { 'Error': 'Could not delete the specified token' })
                    }
                })
            } else {
                callback(400, { 'Error': 'Could not find the specified token' })
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required field' })
    }
}

handlers._tokens.verifyToken = (id, phone, callback) => {
    _data.read('tokens', id, (err, data) => {
        if (!err && data) {
            if (data.phone == phone && data.expires > Date.now()) {
                callback(true)
            } else {
                callback(false)
            }
        } else {
            callback(false)
        }
    })
}

handlers.checks = (data, callback) => {
    let acceptableMethods = ['post', 'get', 'put', 'delete']

    if (acceptableMethods.includes(data.method)) {
        handlers._checks[data.method](data, callback)
    } else {
        callback(405)
    }
}

handlers._checks = {}

handlers._checks.post = (data, callback) => {
    let protocol = typeof data.payload.protocol == 'string' && ['http', 'https'].includes(data.payload.protocol) ? data.payload.protocol : false
    let url = typeof data.payload.url == 'string' && data.payload.url.trim().length > 0 ? data.payload.url.trim() : false
    let method = typeof data.payload.method == 'string' && ['post', 'get', 'put', 'delete'].includes(data.payload.method) ? data.payload.method : false
    let successCodes = typeof data.payload.successCodes == 'object' && data.payload.successCodes instanceof Array && data.payload.successCodes.length > 0 ? data.payload.successCodes : false
    let timeoutSeconds = typeof data.payload.timeoutSeconds == 'number' && data.payload.timeoutSeconds % 1 == 0 && data.payload.timeoutSeconds >= 1 && data.payload.timeoutSeconds <= 5 ? data.payload.timeoutSeconds : false

    if (protocol && url && method && successCodes && timeoutSeconds) {
        let token = typeof data.headers.token == 'string' ? data.headers.token : false

        _data.read('tokens', token, (err, token) => {
            if (!err && token) {
                let userPhone = token.phone

                _data.read('users', userPhone, (err, user) => {
                    if (!err, user) {
                        let checks = typeof user.checks == 'object' && user.checks instanceof Array ? user.checks : []

                        if (checks.length < config.maxChecks) {
                            const parsedUrl = _url.parse(`${protocol}://${url}`, true)

                            const hostname = typeof parsedUrl.hostname === 'string' && parsedUrl.hostname.length > 0 ? parsedUrl.hostname : false

                            dns.resolve(hostname, (error, dnsRecords) => {
                                if (! error && dnsRecords) {
                                    let checkId = helpers.createRandomString(20)

                                    let check = {
                                        'id': checkId,
                                        'userPhone': userPhone,
                                        'protocol': protocol,
                                        'url': url,
                                        'method': method,
                                        'successCodes': successCodes,
                                        'timeoutSeconds': timeoutSeconds
                                    }

                                    _data.create('checks', checkId, check, () => {
                                        if (!err) {
                                            user.checks = checks

                                            user.checks.push(checkId)

                                            _data.update('users', userPhone, user, (error) => {
                                                if (!error) {
                                                    callback(200, check)
                                                } else {
                                                    callback(500, { 'Error': 'Could not update the user with the new check' })
                                                }
                                            })
                                        } else {
                                            callback(500, { 'Error': 'Could not create the new check' })
                                        }
                                    })
                                } else {
                                    callback(400, { 'Error': 'The hostname of the URL entered did not resolve to any DNS entries' })
                                }
                            })
                        } else {
                            callback(400, { 'Error': `The user already has the maximum number of checks ${config.maxChecks}` })
                        }
                    } else {
                        callback(403)
                    }
                })
            } else {
                callback(403)
            }
        })
    } else {
        callback(400, { 'Error': 'Missing the required inputs, or inputs are invalid' })
    }
}

handlers._checks.get = (data, callback) => {
    let id = typeof data.queryStringObject.id == 'string' && data.queryStringObject.id.trim().length == 20 ? data.queryStringObject.id.trim() : false

    if (id) {
        _data.read('checks', id, (error, check) => {
            if (!error && check) {
                let token = typeof data.headers.token == 'string' ? data.headers.token : false

                handlers._tokens.verifyToken(token, check.userPhone, (tokenIsValid) => {
                    if (tokenIsValid) {
                        callback(200, check)
                    } else {
                        callback(403)
                    }
                })
            } else {
                callback(404)
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required field' })
    }
}

handlers._checks.put = (data, callback) => {
    let id = typeof data.payload.id == 'string' && data.payload.id.trim().length == 20 ? data.payload.id.trim() : false
    let protocol = typeof data.payload.protocol == 'string' && ['http', 'https'].includes(data.payload.protocol) ? data.payload.protocol : false
    let url = typeof data.payload.url == 'string' && data.payload.url.trim().length > 0 ? data.payload.url.trim() : false
    let method = typeof data.payload.method == 'string' && ['post', 'get', 'put', 'delete'].includes(data.payload.method) ? data.payload.method : false
    let successCodes = typeof data.payload.successCodes == 'object' && data.payload.successCodes instanceof Array && data.payload.successCodes.length > 0 ? data.payload.successCodes : false
    let timeoutSeconds = typeof data.payload.timeoutSeconds == 'number' && data.payload.timeoutSeconds % 1 == 0 && data.payload.timeoutSeconds >= 1 && data.payload.timeoutSeconds <= 5 ? data.payload.timeoutSeconds : false

    if (id) {
        if (protocol || url || method || successCodes || timeoutSeconds) {
            _data.read('checks', id, (err, check) => {
                if (!err && check) {
                    let token = typeof data.headers.token == 'string' ? data.headers.token : false

                    handlers._tokens.verifyToken(token, check.userPhone, (tokenIsValid) => {
                        if (tokenIsValid) {
                            if (protocol) {
                                check.protocol = protocol
                            }

                            if (url) {
                                check.url = url
                            }

                            if (method) {
                                check.method = method
                            }

                            if (successCodes) {
                                check.successCodes = successCodes
                            }

                            if (timeoutSeconds) {
                                check.timeoutSeconds = timeoutSeconds
                            }

                            _data.update('checks', id, check, (err) => {
                                if (!err) {
                                    callback(200)
                                } else {
                                    callback(500, { 'Error': 'Could not update the check' })
                                }
                            })
                        } else {
                            callback(403)
                        }
                    })
                } else {
                    callback(404, { 'Error': 'Check ID did not exist' })
                }
            })
        } else {
            callback(400, { 'Error': 'Missing fields to update' })
        }
    } else {
        callback(400, { 'Error': 'Missing required field' })
    }
}

handlers._checks.delete = (data, callback) => {
    let id = typeof data.queryStringObject.id == 'string' && data.queryStringObject.id.trim().length == 20 ? data.queryStringObject.id.trim() : false

    if (id) {
        _data.read('checks', id, (error, check) => {
            if (!error && check) {
                let token = typeof data.headers.token == 'string' ? data.headers.token : false

                handlers._tokens.verifyToken(token, check.userPhone, (tokenIsValid) => {
                    if (tokenIsValid) {
                        _data.delete('checks', id, (error) => {
                            if (!error) {
                                _data.read('users', check.userPhone, (err, user) => {
                                    if (!err && user) {
                                        let checks = typeof user.checks == 'object' && user.checks instanceof Array ? user.checks : []

                                        let checkIndex = checks.indexOf(id)

                                        if (checkIndex > -1) {
                                            checks.splice(checkIndex, 1)

                                            _data.update('users', check.userPhone, user, (err) => {
                                                if (!err) {
                                                    callback(200)
                                                } else {
                                                    callback(500, { 'Error': 'Could not update the user' })
                                                }
                                            })
                                        } else {
                                            callback(500, { 'Error': 'Could not find the check on the user\'s object, so could not remove it' })
                                        }
                                    } else {
                                        callback(500, { 'Error': 'Could not find the user who created the check, so could not remove the check from the list of checks on the user object' })
                                    }
                                })
                            } else {
                                callback(500, { 'Error': 'Could not delete the check data' })
                            }
                        })
                    } else {
                        callback(403)
                    }
                })
            } else {
                callback(400, { 'Error': 'The specified check ID does not exist' })
            }
        })
    } else {
        callback(400, { 'Error': 'Missing required field' })
    }
}

handlers.ping = (data, callback) => {
    callback(200)
}

handlers.notFound = (data, callback) => {
    callback(404)
}

module.exports = handlers
