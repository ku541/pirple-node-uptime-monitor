const fs = require('fs')
const path = require('path')
const helpers = require('./helpers')

let lib = {}

lib.baseDir = path.join(__dirname, '/../.data/')

lib.create = (dir, file, data, callback) => {
    let createFilePath = lib.baseDir + dir + '/' + file + '.json'

    fs.open(createFilePath, 'wx', (err, fileDescriptor) => {
        if (!err && fileDescriptor) {
            let stringData = JSON.stringify(data)

            fs.writeFile(fileDescriptor, stringData, (err) => {
                if (!err) {
                    fs.close(fileDescriptor, (err) => {
                        if (!err) {
                            callback(false)
                        } else {
                            callback('Error closing new file')
                        }
                    })
                } else {
                    callback('Error writing to new file')
                }
            })
        } else {
            callback('Could not create new file, it may already exist')
        }
    })
}

lib.read = (dir, file, callback) => {
    let readFilePath = lib.baseDir + dir + '/' + file + '.json'

    fs.readFile(readFilePath, 'utf8', (err, data) => {
        if (!err && data) {
            let parsedData = helpers.parseJsonToObject(data)

            callback(false, parsedData)
        } else {
            callback(err, data)
        }
    })
}

lib.update = (dir, file, data, callback) => {
    let updateFilePath = lib.baseDir + dir + '/' + file + '.json'

    fs.open(updateFilePath, 'r+', (err, fileDescriptor) => {
        if (!err && fileDescriptor) {
            var stringData = JSON.stringify(data)

            fs.ftruncate(fileDescriptor, () => {
                if (fileDescriptor) {
                    fs.writeFile(fileDescriptor, stringData, (err) => {
                        if (!err) {
                            fs.close(fileDescriptor, (err) => {
                                if (!err) {
                                    callback(false)
                                } else {
                                    callback('Error closing existing file')
                                }
                            })
                        } else {
                            callback('Error writing to existing file')
                        }
                    })
                } else {
                    callback('Error truncating file')
                }
            })
        } else {
            callback('Could not open the file for updating, it may not exist yet')
        }
    })
}

lib.delete = (dir, file, callback) => {
    let deleteFilePath = lib.baseDir + dir + '/' + file + '.json'

    fs.unlink(deleteFilePath, (err) => {
        if (!err) {
            callback(false)
        } else {
            callback('Error deleting file')
        }
    })
}

lib.list = (dir, callback) => {
    fs.readdir(lib.baseDir + dir + '/', (error, data) => {
        if (!error && data.length > 0) {
            let trimmedFilenames = []

            data.forEach((fileName) => {
                trimmedFilenames.push(fileName.replace('.json', ''))
            })

            callback(false, trimmedFilenames)
        } else {
            callback(error, data)
        }
    })
}

module.exports = lib