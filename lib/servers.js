const fs = require('fs')
const url = require('url')
const http = require('http')
const path = require('path')
const https = require('https')
const StringDecoder = require('string_decoder').StringDecoder
const config = require('./config')
const helpers = require('./helpers')
const handlers = require('./handlers')
const debug = require('util').debuglog('server')

let servers = {}

servers.httpServer = http.createServer((req, res) => {
    servers.unifiedServer(req, res)
})

servers.httpsServerOptions = {
    'key': fs.readFileSync(path.join(__dirname, '/../https/key.pem')),
    'cert': fs.readFileSync(path.join(__dirname, '/../https/cert.pem'))
}

servers.httpsServer = https.createServer(servers.httpsServerOptions, (req, res) => {
    servers.unifiedServer(req, res)
})

servers.unifiedServer = (req, res) => {
    let parsedUrl = url.parse(req.url, true)

    let path = parsedUrl.pathname
    let trimmedPath = path.replace(/^\/+|\/+$/g, '')

    let queryStringObject = parsedUrl.query

    let method = req.method.toLowerCase()

    let headers = req.headers

    let decoder = new StringDecoder('utf8')
    let buffer = ''

    req.on('data', (data) => {
        buffer += decoder.write(data)
    })

    req.on('end', () => {
        buffer += decoder.end()

        let chosenHandler = typeof servers.router[trimmedPath] !== 'undefined'
            ? servers.router[trimmedPath] : handlers.notFound

        chosenHandler = trimmedPath.includes('public/') ? handlers.public : chosenHandler

        let data = {
            'trimmedPath': trimmedPath,
            'queryStringObject': queryStringObject,
            'method': method,
            'headers': headers,
            'payload': helpers.parseJsonToObject(buffer)
        }

        try {
            chosenHandler(data, (statusCode, payload, contentType) => {
                servers.processHandlerResponse(res, method, trimmedPath, statusCode, payload, contentType)
            })
        } catch (error) {
            debug(error)

            servers.processHandlerResponse(res, method, trimmedPath, 500, {
                'Error': 'An unknown error has occured'
            }, 'json')
        }
    })
}

servers.processHandlerResponse = (res, method, trimmedPath, statusCode, payload, contentType) => {
    statusCode = typeof statusCode == 'number' ? statusCode : 200

    contentType = typeof contentType == 'string' ? contentType : 'json'

    let payloadString = ''

    if (contentType == 'json') {
        res.setHeader('Content-Type', 'application/json')

        payload = typeof payload == 'object' ? payload : {}

        payloadString = JSON.stringify(payload)
    }

    if (contentType == 'html') {
        res.setHeader('Content-Type', 'text/html')

        payloadString = typeof payloadString == 'string' ? payload : ''
    }

    if (contentType == 'favicon') {
        res.setHeader('Content-Type', 'image/x-icon')

        payloadString = typeof payloadString !== 'undefined' ? payload : ''
    }

    if (contentType == 'css') {
        res.setHeader('Content-Type', 'text/css')

        payloadString = typeof payloadString !== 'undefined' ? payload : ''
    }

    if (contentType == 'png') {
        res.setHeader('Content-Type', 'image/png')

        payloadString = typeof payloadString !== 'undefined' ? payload : ''
    }

    if (contentType == 'jpg') {
        res.setHeader('Content-Type', 'image/jpeg')

        payloadString = typeof payloadString !== 'undefined' ? payload : ''
    }

    if (contentType == 'plain') {
        res.setHeader('Content-Type', 'text/plain')

        payloadString = typeof payloadString !== 'undefined' ? payload : ''
    }

    res.writeHead(statusCode)
    res.end(payloadString)

    // console.log(
    //     '\x1b[36m%s\x1b[0m',
    //     `Method\t\t: ${method}\n` +
    //     `Path\t\t: ${trimmedPath}\n` +
    //     `Query\t\t: ${JSON.stringify(queryStringObject)}\n` +
    //     `Headers\t\t: ${JSON.stringify(headers)}\n` +
    //     `Payload\t\t: ${JSON.stringify(buffer)}\n` +
    //     `Status\t\t: ${statusCode}\n` +
    //     `Response\t: ${payloadString}\n`
    // )
}

servers.router = {
    '': handlers.index,
    'account/create': handlers.accountCreate,
    'account/edit': handlers.accountEdit,
    'account/deleted': handlers.accountDeleted,
    'session/create': handlers.sessionCreate,
    'session/deleted': handlers.sessionDeleted,
    'checks/all': handlers.checksList,
    'checks/create': handlers.checksCreate,
    'checks/edit': handlers.checksEdit,
    'ping': handlers.ping,
    'api/users': handlers.users,
    'api/tokens': handlers.tokens,
    'api/checks': handlers.checks,
    'favicon.ico': handlers.favicon,
    'public': handlers.public,
    'examples/error': handlers.exampleError
}

servers.init = () => {
    servers.httpServer.listen(config.httpPort, () => {
        console.log(
            '\x1b[32m%s\x1b[0m',
            `The http server is listening on port ${config.httpPort}`
        )
    })

    servers.httpsServer.listen(config.httpsPort, () => {
        console.log(
            '\x1b[32m%s\x1b[0m',
            `The https server is listening on port ${config.httpsPort}`
        )
    })
}

module.exports = servers