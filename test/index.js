const unit = require('./unit')
const api = require('./api')

process.env.NODE_ENV = 'testing'

_app = {}

_app.tests = {
    'unit': {},
    'api': {}
}

_app.tests.unit = unit

_app.tests.api = api

_app.countTests = () => {
    let testCount = 0
    
    for (const testLevel in _app.tests) {
        if (Object.hasOwnProperty.call(_app.tests, testLevel)) {
            const levelTests = _app.tests[testLevel]
            
            for (testName in levelTests) {
                if (Object.hasOwnProperty.call(levelTests, testName)) {
                    testCount++
                }
            }
        }
    }

    return testCount
}

_app.runTests = () => {
    let iteration = 0
    let passed = 0

    const errors = []
    const limit = _app.countTests()

    for (const testLevel in _app.tests) {
        if (Object.hasOwnProperty.call(_app.tests, testLevel)) {
            const levelTests = _app.tests[testLevel]

            for (const testName in levelTests) {
                if (Object.hasOwnProperty.call(levelTests, testName)) {
                    const testValue = levelTests[testName]

                    try {
                        testValue(() => {
                            console.log('\x1b[32m%s\x1b[0m', testName)

                            iteration++
                            passed++

                            if (iteration === limit) {
                                _app.produceTestReport(limit, passed, errors)
                            }
                        })
                    } catch (error) {
                        errors.push({
                            'name': testName,
                            'message': error
                        })

                        console.log('\x1b[31m%s\x1b[0m', testName)

                        iteration++

                        if (iteration === limit) {
                            _app.produceTestReport(limit, passed, errors)
                        }
                    }
                }
            }
        }
    }
}

_app.produceTestReport = (limit, passed, errors) => {
    console.log('')
    console.log('----------BEGIN TEST REPORT----------')
    console.log('')
    console.log('Total Tests:', limit)
    console.log('Passed: ', passed)
    console.log('Failed: ', errors.length)

    if (errors.length > 0) {
        console.log('')
        console.log('----------BEGIN ERROR DETAILS----------')
        console.log('')
        errors.forEach(error => {
            console.log('\x1b[31m%s\x1b[0m', error.name)
            console.log(error.message);
            console.log('');
        })
        console.log('----------END ERROR DETAILS----------')
    }

    console.log('')
    console.log('----------END TEST REPORT----------')

    process.exit(0)
}

_app.runTests()