const app = require('../index')
const assert = require('assert')
const http = require('http')
const config = require('../lib/config')

const api = {}

const helpers = {}

helpers.makeGetRequest = (path, callback) => {
    const requestDetails = {
        'protocol': 'http:',
        'hostname': 'localhost',
        'port': config.httpPort,
        'method': 'GET',
        'path': path,
        'headers': {
            'Content-Type': 'application/json'
        }
    }

    const request = http.request(requestDetails, (response) => {
        callback(response)
    })

    request.end()
}

api['app.init should start without throwing'] = (done) => {
    assert.doesNotThrow(() => {
        app.init(() => {
            done()
        })
    }, TypeError)
}

api['/ping should respond to GET with 200'] = (done) => {
    helpers.makeGetRequest('/ping', (response) => {
        assert.equal(response.statusCode, 200)

        done()
    })
}

api['/api/users should respond to GET with 400'] = (done) => {
    helpers.makeGetRequest('/api/users', (response) => {
        assert.equal(response.statusCode, 400)

        done()
    })
}

api['A random path should respond to GET with 404'] = (done) => {
    helpers.makeGetRequest('/random/path', (response) => {
        assert.equal(response.statusCode, 404)

        done()
    })
}

module.exports = api;