const helpers = require('../lib/helpers')
const assert = require('assert')
const _logs = require('../lib/logs')

const unit = {}

unit['logs.list should callback a false error and an array of log names'] = (done) => {
    _logs.list(true, (error, logFilenames) => {
        assert.equal(error, false)
        assert.ok(logFilenames instanceof Array)
        assert.ok(logFilenames.length > 1)

        done()
    })
}

unit['logs.truncate should callback an error without throwing if the logId does not exist'] = (done) => {
    assert.doesNotThrow(() => {
        _logs.truncate('I do not exist', (error) => {
            assert.ok(error)

            done()
        })
    }, TypeError)
}

module.exports = unit